﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DungeonReportScript : MonoBehaviour {
	public Text reportText;
	public Text resultText;
	public Text lootText;
	public DungeonManagerScript dungManager;
	public CharacterStats chara;
	private int g,xpe,sta;
	public ItemScript item;
	public ItemGenerationScript itemGen;
	// Use this for initialization
	void Start () {
		chara = GameObject.Find ("PlayerStats").GetComponent<CharacterStats> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Ending(bool winOrLose, int gold, int xp, int staminaloss){
		g =gold;
		xpe = xp;
		sta = staminaloss;

		if (winOrLose) {
			item = itemGen.GenerateItem();
			Debug.Log(item.name);
			//item.AddItemInfo(itemGen.item);
			resultText.text = "Result: " +"You Cleared the Dungeon!";
			reportText.text = "Report: " + "You cleared your way into the deepest parts of the dungeon. " + "\n"+ 
				"And you found the boss of the floor and you showed" + "\n"+ " him the way out of the Dungeon, You ROCK!";
			lootText.text = "Loot: " + "Gold: "+ gold + " Experience: " + xp + "\n"+ "Lost Stamina: " + staminaloss;
			lootText.text += " Item: " + item.name;

		} else {
			resultText.text = "Result: " + "You Almost Died in the Dungeon!";
			reportText.text = "Report: " + "You almost died in the dungeon floor! Go train some more or you will be killed next time";
			lootText.text = "Loot: " + "Gold: "+ gold + " Experience: " + xp + "\n"+  "Lost Stamina: " + staminaloss;

		}
	
	}

	public void ExitReport(){
		chara.AddLoot (xpe, g, sta);
		dungManager.dungPhase = DungeonPhase.CloseReport;
		dungManager.DungeonPhaseManagement ();
		dungManager.dungPhase = DungeonPhase.OpenStats;
		dungManager.DungeonPhaseManagement ();
	}

	public void GenerateLootItem(){

	}
}
