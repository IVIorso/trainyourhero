﻿using UnityEngine;
using System.Collections;

public class EquipAndSellScript : MonoBehaviour {
	public CharacterStats charra;
	public ItemScript item;
	public InventoryScript inv;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void Sell(){

	
		for (int a = 0; a < charra.inventory.Count; a ++) {
			if(charra.inventory[a].id == item.id){
				Debug.Log ("Selling item: " + charra.inventory[a].name + " with id: " + charra.inventory[a].id);
				charra.inventory.Remove (charra.inventory[a]);
				charra.gold += item.value;
				inv.UpdateInventory();
			}
		}
		//Debug.Log ("Inv Id: " + i);
	
	}

	public void EquipItem(){
		ItemScript itemToput = new ItemScript ();
		
		int i = item.id;
	
		
		switch (item.slot) {
		case Slot.Chest:
			if(charra.equipment[Slot.Chest].name != ""){ 
				itemToput.AddItemInfo(charra.equipment[Slot.Chest]);
				charra.equipment[Slot.Chest].AddItemInfo(item);
				charra.inventory[i].AddItemInfo(itemToput);

			}
			else{
				charra.equipment[Slot.Chest].AddItemInfo(item);
			
			
			}
			inv.UpdateInventory();
			break;
		case Slot.Head:
			if(charra.equipment[Slot.Head].name != ""){ 
				itemToput.AddItemInfo(charra.equipment[Slot.Head]);
				charra.equipment[Slot.Head].AddItemInfo(item);
				charra.inventory[i].AddItemInfo(itemToput);
			}
			else{
				charra.equipment[Slot.Head].AddItemInfo(item);
			}
			inv.UpdateInventory();
			break;
		case Slot.Legs: 
			if(charra.equipment[Slot.Legs].name != ""){ 
				itemToput.AddItemInfo(charra.equipment[Slot.Legs]);
				charra.equipment[Slot.Legs].AddItemInfo(item);
				charra.inventory[i].AddItemInfo(itemToput);
			}
			else{
				charra.equipment[Slot.Legs].AddItemInfo(item);
			}
			inv.UpdateInventory();
			break;
		case Slot.Weapon:

			if(charra.equipment[Slot.Weapon].name != ""){ 
				itemToput.AddItemInfo(charra.equipment[Slot.Weapon]);
				charra.equipment[Slot.Weapon].AddItemInfo(item);
				charra.inventory[i].AddItemInfo(itemToput);
			}
			else{
				charra.equipment[Slot.Weapon].AddItemInfo(item);
				charra.inventory.RemoveAt(i);
			}
			Debug.Log ("Equiping item: " + charra.inventory[i].name + " with id: " + charra.inventory[i].id);
			inv.UpdateInventory();
			break;
		default:
			break;
		}
	}
}
