﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectItemScript : MonoBehaviour {

	public InventoryScript inv;
	public ItemScript item;
	public EquipAndSellScript equip;
	public EquipAndSellScript sell;
	public Image img;
	// Use this for initialization
	void Start () {
		equip = GameObject.Find ("EquipButton").GetComponent<EquipAndSellScript>();
		sell = GameObject.Find ("SellButton").GetComponent<EquipAndSellScript>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SelectThisItem(){
		Debug.Log ("Item selected: " +item.name + " With id: " + item.id);
		inv.item.AddItemInfo (item);
		equip.item.AddItemInfo (item);
		sell.item.AddItemInfo (item);
		img.sprite = item.icon;
	}

}
