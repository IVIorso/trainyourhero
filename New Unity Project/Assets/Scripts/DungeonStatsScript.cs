﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DungeonStatsScript : MonoBehaviour {

	public string DungeonTheme;

	public int damage, defence, lvlRequired, id, dungeonXP, gold, staminaloss;
	public string name;
	public Text nameText, damageText, defenceText, lvlrequiredText, dungeonThemeText;
	private CharacterStats chara;

	public ExploreDungeonScript dungeon;
	public DungeonStatsScript thisDungeon;

	public DungeonManagerScript dungManager;
	

	// Use this for initialization
	void Start () {

		chara = GameObject.Find ("PlayerStats").GetComponent<CharacterStats>();
		thisDungeon = gameObject.GetComponent<DungeonStatsScript> ();
		dungManager.dungPhase = DungeonPhase.OpenStats;
		dungManager.DungeonPhaseManagement ();
		dungManager.dungPhase = DungeonPhase.CloseReport;
		dungManager.DungeonPhaseManagement ();


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator UnlockDungeon(){
		yield return new WaitForSeconds (0.01f);

		if (chara.level < lvlRequired) {
			gameObject.SetActive(false);
		}
	}


	public void SetDungeonStats(){

		nameText.text = "Floor name: " + name;
		defenceText.text = "Defence: " + defence;
		damageText.text = "Damage: " + damage;
		lvlrequiredText.text = "Level Required: " + lvlRequired;
		dungeonThemeText.text = "Dungeon Name: " + DungeonTheme;
		dungeon.dung = thisDungeon;
	}



}
