﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BuildingScript : MonoBehaviour {


	public BuildingUpgradeScript bUpgScript;
	public CharacterStats chara;
	public string buildingName;
	public int buildingLvl;
	public int buildingCost;
	public int staminaCost;
	public int expGain;
	public int goldGain;
	public int costIncreasePerLvl;
	public int expGainIncrease;
	public int goldGainIncrease;

	public int nextBuildingLvl;
	public int nextStaminaCost;
	public int nextExpGain;
	public int nextGoldGain;
	
	public Text buildingNameText;
	public Text currentBuildingLvlText;
	public Text currentStaminaCostText;
	public Text currentGoldGainText;
	public Text currentExpGainText;
	
	public Text nextStaminaCostText;
	public Text nextGoldGainText;
	public Text nextExpGainText;

	public Text currentBuildingCostText;
	public Text goldAmount;
	// Use this for initialization
	void Start () {
	
	}

	public void ShowInfo()
	{
		buildingNameText.text = buildingName;
		currentBuildingLvlText.text = "Lvl: " +buildingLvl;
		currentStaminaCostText.text = "Stamina cost: " + staminaCost;
		currentGoldGainText.text = "Gold gain: " + goldGain;
		currentExpGainText.text = "Exp gain: " + expGain;

		nextStaminaCostText.text = "Stamina cost: " + nextStaminaCost;
		nextGoldGainText.text = "Gold gain: " + nextGoldGain;
		nextExpGainText.text = "Exp gain: " + nextExpGain;
		
		currentBuildingCostText.text = "Upgrade Cost: " + buildingCost;
		bUpgScript.buildingName = buildingName;
	}

	public void Upgrade()
	{
		if (chara.gold >= buildingCost) {
			chara.gold = chara.gold - buildingCost;
			buildingCost = buildingCost + costIncreasePerLvl;
			expGain = expGain+expGainIncrease;
			nextExpGain = expGain+expGainIncrease;
			goldGain = goldGain + goldGainIncrease;
			nextGoldGain = goldGain + goldGainIncrease;
			buildingLvl++;
			ShowInfo ();
			goldAmount.text = "Gold: " + chara.gold;
		}
	}
}
