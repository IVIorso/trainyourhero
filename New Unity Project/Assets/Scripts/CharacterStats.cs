﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Gender{
	Female,
	Male
}

public class CharacterStats : MonoBehaviour {
	public PlayerActionScript plAction;
	public string name;
	public Gender gender;
	public int physicalDamage, magicalDamage, currentHp, maxHp, currentStamina, maxStamina, armor, level, daysPassed, currentXp, xpToLevelUp, gold;
	public Sprite icon;
	public Dictionary<Slot, ItemScript> equipment = new Dictionary<Slot, ItemScript>();
	public List<ItemScript> inventory = new List<ItemScript>();

	public ItemGenerationScript itemGe;
	public ItemScript itemi;
	public InventoryScript inv;

	// Use this for initialization
	void Start () {
		plAction = GameObject.Find ("PlayerActionScript").GetComponent<PlayerActionScript> ();
		equipment.Add (Slot.Weapon, new ItemScript ());
		equipment.Add (Slot.Head, new ItemScript ());
		equipment.Add (Slot.Chest, new ItemScript ());
		equipment.Add (Slot.Legs, new ItemScript ());


		
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void AddLoot(int xp, int coins, int staminaloss){
		gold += coins;
		plAction.CountXP (xp);
		plAction.AddjustStamina (-staminaloss);
	}

	public void CalculateStats(){

	}

	public void GetStats(){

	}
}
