﻿using UnityEngine;
using System.Collections;


public enum ItemType{
	Weapon,
	Armor
}

public enum Slot{
	Weapon,
	Head,
	Chest,
	Legs
}
public enum ArmorType{
	None,
	Iron,
	Leather,
	Silk
}

public enum WeaponType{
	None,
	Sword,
	Axe,
	Crossbow,
	Bow,
	Staff,
	Book
}

public enum Rarity{
	Common,	//white
	Magic,	//blue
	Rare,	//yellow
	Legendary	//Orange
}

public class ItemScript : MonoBehaviour {
	public int id = 0;
	public string name;
	public int value;
	public string description;
	public int specialValue; //weapon damage or armors armor;
	public Slot slot;
	public Rarity rarity;
	public ItemType itemType;
	public ArmorType armType;
	public WeaponType wepType;
	public Color col;
	public Sprite icon;
	public ShowStats show;
	private ItemScript item;
	private CharacterStats charstats;

	void Start(){
		item = gameObject.GetComponent<ItemScript> ();
		charstats = GameObject.Find ("PlayerStats").GetComponent<CharacterStats> ();
	}

	public void AddItemInfo(ItemScript i){
		name = i.name;
		value = i.value;
		description = i.description;
		specialValue = i.specialValue;
		slot = i.slot;
		rarity = i.rarity;
		itemType = i.itemType;
		wepType = i.wepType;
		armType = i.armType;
		icon = i.icon;
		col = i.col;
		id = i.id;
		


	}

	public void ShowItem(){
		//show.itemi.AddItemInfo(charstats.equipment[slot]);
	//	show.itemi.AddItemInfo (item);
	//	show.ShowItemStats ();
	}
}
