﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ExploreDungeonScript : MonoBehaviour {

	public DungeonStatsScript dung;
	public CharacterStats chara;
	public DungeonReportScript report;

	public int characterMinRoll,  dungeonMinRoll, characterRoll, dungeonRoll, maxRoll;
	public DungeonManagerScript dungManager;


	// Use this for initialization
	void Start () {
		maxRoll = 100;
		chara = GameObject.Find ("PlayerStats").GetComponent<CharacterStats> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void CalcuteDungeon(){
		if (chara.currentStamina >= dung.staminaloss) {
			//which is better player damage or dung defence
			
			if (chara.physicalDamage > dung.defence) {
				//character win + 5 * minus
				characterMinRoll += (5 * (chara.physicalDamage - dung.defence));
			} else {
				dungeonMinRoll += (5 * (dung.defence - chara.physicalDamage));
			}
			
			//which is better player Def or dung damage
			
			if (chara.armor > dung.damage) {
				characterMinRoll += (5 * (chara.armor - dung.damage));
				
			} else {
				dungeonMinRoll += (5 * (dung.damage - chara.armor));
			}
			
			
			//how much is character level over the dung level
			characterMinRoll += (5 * (chara.level - dung.lvlRequired));
			
			
			
			
			//roll for the win
			characterRoll = Random.Range (characterMinRoll, maxRoll);
			dungeonRoll = Random.Range (dungeonMinRoll, maxRoll);
			
			if (characterRoll > dungeonRoll) {
				Debug.Log ("Win");
				dungManager.dungPhase = DungeonPhase.OpenReport;
				dungManager.DungeonPhaseManagement ();
				report.Ending (true, dung.gold, dung.dungeonXP, dung.staminaloss);
				dungManager.dungPhase = DungeonPhase.CloseStats;
				dungManager.DungeonPhaseManagement ();
			} else {
				Debug.Log ("Lose");
				dungManager.dungPhase = DungeonPhase.OpenReport;
				dungManager.DungeonPhaseManagement ();
				report.Ending (false, 0, 0, dung.staminaloss);
				dungManager.dungPhase = DungeonPhase.CloseStats;
				dungManager.DungeonPhaseManagement ();
			}
		} else {
			gameObject.GetComponentInChildren<Text>().text = "Not enough Stamina";
		}

	}
}
