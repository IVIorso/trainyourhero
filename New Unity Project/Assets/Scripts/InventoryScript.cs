﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class InventoryScript : MonoBehaviour {

	public CharacterStats charra;
	public ItemScript item;
	public GameObject inventory;
	public GameObject invSlot;
	public List<SelectItemScript> slotSelects = new List<SelectItemScript>();
	public List<GameObject> slots = new List<GameObject>();
	public int invY;
	public int invX;

	public Sprite defaultSprite;
	public Color defaultColor;
	public ItemScript defaultItemScript;

	// Use this for initialization
	void Start () {
		charra = GameObject.Find ("PlayerStats").GetComponent<CharacterStats> ();
		CreateInventoryGrid ();
		defaultItemScript = new ItemScript ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CreateInventoryGrid(){

		for (int i = 0; i < invY; i++) {
			for(int a = 0; a < invX; a++){
				GameObject invSlo = (GameObject)Instantiate(invSlot);
				invSlo.transform.SetParent(inventory.transform);
				invSlo.GetComponent<RectTransform>().localPosition = new Vector2(-185 + a * 53f,131 + -i * 53f);
				invSlo.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
				SelectItemScript invlo =  new SelectItemScript();
				invlo = invSlo.GetComponent<SelectItemScript>();
				invlo.inv = gameObject.GetComponent<InventoryScript>();
				invSlo.SetActive(false);
				slotSelects.Add(invlo);
				slots.Add(invSlo);
				
			}
		}
		UpdateInventory ();
	}

	public void UpdateInventory(){
		int id = 0;
		for(int i = 0; i < charra.inventory.Count; i++){
			slots[i].SetActive(true);
			slotSelects[i].img.sprite = charra.inventory[i].icon;
			slotSelects[i].GetComponent<Image>().color =charra.inventory[i].col;
			charra.inventory[i].id = i;
			slotSelects[i].item.id = i;
			slotSelects[i].item.AddItemInfo(charra.inventory[i]);
			id++;
		}
		for (int a = id; a < ( invX * invY); a++) {
			slots[a].SetActive(false);
		
		}
			

		}





}
