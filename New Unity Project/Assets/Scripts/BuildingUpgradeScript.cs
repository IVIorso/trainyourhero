﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BuildingUpgradeScript : MonoBehaviour {

	public PlayerActionScript plAc;
	public BuildingScript trainingGroundsScript;
	public BuildingScript bakeryScript;
	public string buildingName;
	// Use this for initialization
	void Start () {
	
	}
	
	public void UpgradeBuilding()
	{
	
		switch (buildingName) {
		case "Training Grounds":
			trainingGroundsScript.Upgrade();
			plAc.SetNewBuildingStats(bakeryScript, trainingGroundsScript);
			break;
		case "Bakery":
			bakeryScript.Upgrade();
			plAc.SetNewBuildingStats(bakeryScript, trainingGroundsScript);
			break;
		}
	}
}
