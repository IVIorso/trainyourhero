﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerActionScript : MonoBehaviour {

	private CharacterStats chara;
	public Text heroName;
	public Text heroHealth;
	public Text heroStamina;
	public Text currentDay;
	public Text heroExp;
	public Text heroLevel;
	public int expGainOnTrain = 30;
	public int trainStamCost = 10;
	public int bakeStamCost = 5;
	public int goldGained = 10;
	public Text goldAmount;
	public Text heroDamage;
	public Text heroArmor;

	// Use this for initialization
	void Start () {
		chara = GameObject.Find ("PlayerStats").GetComponent<CharacterStats> ();
		heroName.text = chara.name;
		currentDay.text = "Day: " + chara.daysPassed;
		heroLevel.text = "Lvl: " + chara.level;
		heroHealth.text = "Health: " + chara.currentHp+ "/" + chara.maxHp;
		heroStamina.text = "Stamina: " + chara.currentStamina+ "/" + chara.maxStamina;
		heroExp.text = "Exp: "+ chara.currentXp + "/" + chara.xpToLevelUp;
		goldAmount.text = "Gold: " + chara.gold;
		heroDamage.text = "Damage: " + chara.physicalDamage;
		heroArmor.text = "Defence: " + chara.armor;
	
	}
	
	public void Train()
	{
		if ( chara.currentStamina >= trainStamCost) {
			AddjustStamina(-trainStamCost);
			CountXP(expGainOnTrain);
			heroStamina.text = "Stamina: " +  chara.currentStamina+ "/" + chara.maxStamina;

			if (chara.currentXp >= chara.xpToLevelUp) {
				LevelUp();
			}
			heroExp.text = "Exp: "+ chara.currentXp + "/" +chara.xpToLevelUp;
		}
	}
	public void AddjustStamina(int adj){
		chara.currentStamina += adj;
	}

	public void CountXP(int xpgained){
		chara.currentXp += xpgained;
	}

	public void LevelUp(){
		chara.currentXp =chara.currentXp - chara.xpToLevelUp;
		chara.xpToLevelUp = chara.level*65+125;
		chara.level++;
		chara.armor++;
		chara.physicalDamage += 2;
		heroLevel.text = "Lvl: " + chara.level;
		chara.currentStamina = chara.maxStamina;
		heroStamina.text = "Stamina: " + chara.currentStamina + "/" + chara.maxStamina;
		heroDamage.text = "Damage: " + chara.physicalDamage;
		heroArmor.text = "Defence: " + chara.armor;
	}
	public void Bake()
	{
		if (chara.currentStamina >= bakeStamCost) {
			chara.currentStamina = chara.currentStamina-bakeStamCost;
			heroStamina.text = "Stamina: " + chara.currentStamina+ "/" + chara.maxStamina;
			chara.gold += goldGained;
			goldAmount.text = "Gold: " + chara.gold;
		}
	}
	public void Rest()
	{
		chara.currentStamina = chara.maxStamina;
		heroStamina.text = "Stamina: " + chara.currentStamina+ "/" + chara.maxStamina;
		chara.daysPassed ++;
		currentDay.text = "Day: " + chara.daysPassed;
	}

	public void SetNewBuildingStats(BuildingScript bakery, BuildingScript training){
		bakeStamCost = bakery.staminaCost;
		goldGained = bakery.goldGain;
		trainStamCost = training.staminaCost;
		expGainOnTrain = training.expGain;

		
	}

	public void GetStats(){
		heroName.text = chara.name;
		currentDay.text = "Day: " + chara.daysPassed;
		heroLevel.text = "Lvl: " + chara.level;
		heroHealth.text = "Health: " + chara.currentHp+ "/" + chara.maxHp;
		heroStamina.text = "Stamina: " + chara.currentStamina+ "/" + chara.maxStamina;
		heroExp.text = "Exp: "+ chara.currentXp + "/" + chara.xpToLevelUp;
		goldAmount.text = "Gold: " + chara.gold;
		heroDamage.text = "Damage: " + chara.physicalDamage;
		heroArmor.text = "Defence: " + chara.armor;
	}

	public void Quit(){
		Application.Quit();
	}
}
