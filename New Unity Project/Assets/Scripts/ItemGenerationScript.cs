﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ItemGenerationScript : MonoBehaviour{
	//public ItemGenerationScript itemGen;
	public ItemScript item;
	//public ShowStats show;
	public Color[] colours = new Color[4];
	// Use this for initialization
	void Start () {
		item = new ItemScript ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public ItemScript GenerateItem(){
	
		int rand = Random.Range (0, 50);
		int a = 0;
		if (rand <= 24) {
			a = 0;
		} else if (rand > 24) {
			a = 1;
		}
		switch (a) {
		case 0:
			item = CreateWeapon();	
			return item;
			break;
		case 1: 
			item = CreateArmor();
			return item;
			break;
		default:
			item = CreateWeapon();
			return item;
			break;
		}

	

	}

	public ItemScript CreateWeapon(){
		item.icon = Resources.Load <Sprite>("sword");
		item.itemType = ItemType.Weapon;
		item.armType = ArmorType.None;
		item.slot = Slot.Weapon;
		int rand = Random.Range (0, 4);
		item.wepType = WeaponType.Sword;
		float spValue = 0;
		switch (rand) {
		case 0:
			item.rarity = Rarity.Common;
			item.name = "Common Iron Sword";
			item.value = 10;
			item.description = "Common Iron Sword";
			spValue = 5;
			item.specialValue = (int)(Random.Range(spValue*0.8f, spValue+1));
			item.col = colours[0];
			return item;
			break;
		case 1:
			item.rarity = Rarity.Magic;
			item.name = "Magical Iron Sword";
			item.value = 50;
			item.description = "Magical Iron Sword";
			spValue = 10;
			item.specialValue = (int)(Random.Range(spValue*0.8f, spValue+1));
			item.col = colours[1];
			return item;
			break;
		case 2:
			item.rarity = Rarity.Rare;
			item.name = "Rare Iron Sword";
			item.value = 100;
			item.description = "Rare Iron Sword";
			spValue = 20;
			item.specialValue = (int)(Random.Range(spValue*0.8f, spValue+1));
			item.col = colours[2];
			return item;
			break;
		case 3:
			item.rarity = Rarity.Common;
			item.name = "Legendary Iron Sword";
			item.value = 1000;
			item.description = "Legendary Iron Sword";
			spValue = 50;
			item.specialValue = (int)(Random.Range(spValue*0.8f, spValue+1));
			item.col = colours[3];
			return item;
			break;
		default:
			item.rarity = Rarity.Common;
			item.name = "Common Iron Sword";
			item.value = 10;
			item.description = "Common Iron Sword";
			item.specialValue = 5;
			item.col = colours[0];
			return item;
			break;
		}


	}



	public ItemScript CreateArmor(){
		item.itemType = ItemType.Armor;
		item.armType = ArmorType.Iron;
		item.wepType = WeaponType.None;
		int armi = Random.Range (0, 3);
		if (armi == 0) {
			item.slot = Slot.Chest;
			item.icon = Resources.Load<Sprite>("platechest");
			item.name = "Iron Platechest";
		}
		if (armi == 1) {
			item.slot = Slot.Head;
			item.icon = Resources.Load<Sprite>("platehead");
			item.name = "Iron Platehelm";
		}
		if (armi == 2) {
			item.slot = Slot.Legs;
			item.icon = Resources.Load<Sprite>("platelegs");
			item.name = "Iron Platelegs";
		}

		int rari = Random.Range (0, 4);
		float spValue = 0;
		switch (rari) {
		case 0:
			item.rarity = Rarity.Common;
			item.name = "Common " + item.name;
			item.value = 10;
			item.description = item.name;
			spValue = 5;
			item.specialValue = (int)(Random.Range(spValue*0.8f, spValue+1));
			item.col = colours[0];
			return item;
			break;
		case 1:
			item.rarity = Rarity.Magic;
			item.name = "Magical "+ item.name;;
			item.value = 50;
			item.description = item.name;;
			spValue = 10;
			item.specialValue = (int)(Random.Range(spValue*0.8f, spValue+1));
			item.col = colours[1];
			return item;
			break;
		case 2:
			item.rarity = Rarity.Rare;
			item.name = "Rare " + item.name;;
			item.value = 100;
			item.description = item.name;
			spValue = 20;
			item.specialValue = (int)(Random.Range(spValue*0.8f, spValue+1));
			item.col = colours[2];
			return item;
			break;
		case 3:
			item.rarity = Rarity.Common;
			item.name = "Legendary "+ item.name;
			item.value = 1000;
			item.description =  item.name;
			spValue = 50;
			item.specialValue = (int)(Random.Range(spValue*0.8f, spValue+1));
			item.col = colours[3];
			return item;
			break;
		default:
			item.rarity = Rarity.Common;
			item.name = "Common " + item.name;
			item.value = 10;
			item.description = item.name;
			item.specialValue = 5;
			item.col = colours[0];
			return item;
			break;
		}
	}


}
