﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum DungeonPhase{
	CloseStats,
	OpenStats,
	CloseReport,
	OpenReport
}

public class DungeonManagerScript : MonoBehaviour {

	public CharacterStats chara;
	public DungeonPhase dungPhase;

	public GameObject dungStats;
	public GameObject dungReport;
	
	public DungeonStatsScript[] dungLevels = new DungeonStatsScript[0];
	public Text exploreButtonText;

	// Use this for initialization
	void Start () {
		chara = GameObject.Find ("PlayerStats").GetComponent<CharacterStats> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpeningDungeonInterface(){
		for(int i = 0; i < dungLevels.Length; i++){
			dungLevels[i].gameObject.SetActive(true);
		}
		for(int i = 0; i < dungLevels.Length; i++){
			dungLevels[i].StartCoroutine(dungLevels[i].UnlockDungeon());
		}
		exploreButtonText.text = "Explore Dungeon!";
	}

	public void DungeonPhaseManagement(){
		switch (dungPhase) {
		case DungeonPhase.OpenStats:
			Debug.Log("opening stats");
			dungStats.SetActive(true);
			break;
		case DungeonPhase.CloseStats:
			Debug.Log("closing stats");
			dungStats.SetActive(false);
			break;
		case DungeonPhase.CloseReport:
			Debug.Log("Closing report");
			dungReport.SetActive(false);
			break;
		case DungeonPhase.OpenReport:
			Debug.Log("opening report");
			dungReport.SetActive(true);
			break;


		default:
			break;
		}
	}



}
